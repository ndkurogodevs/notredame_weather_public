{object_wrapper}
    <div class="kgo_inset kgoui_detail_header">
        {field_is_set_wrapper field="thumbnail" class="kgoui_detail_thumbnail"}

        {field_is_set_wrapper field="title" tag="h1" class="kgoui_detail_subtitle"}

		{field_is_set_wrapper field="temp" tag="p" class="kgoui_detail_title"}

		{field_is_set_wrapper field="subtitle" tag="p" class="kgoui_detail_title"}
    </div>

    {include region="actions"}

    {include region="content"}

{/object_wrapper}